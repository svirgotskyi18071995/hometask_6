# Наришіть декоратор, який вимірює час виконання функці
tsk1 = '---------------------------------------------------- Task 1 -----------------------------------------------------'
space = '-----------------------------------------------------------------------------------------------------------------'
print(space)
print(tsk1)
print(space)
import time
def time_decorator(function):
    def wrapper(*args, **kwargs):
        first_time = time.time()
        function(*args, **kwargs)
        second_time = time.time()
        delta_time = second_time - first_time
        print(f'Spent time ---->{delta_time}')
    return wrapper
@time_decorator
def calculation():
    arg1 = int(input('Type please first argument.\n'))
    arg2 = int(input('Type please second argument.\n'))
    arg3 = int(input('Type please third argument.\n'))
    print('Choose the operation:\n 1 - sum \n 2 - difference\n 3 - multiplication \n 4 - division \n')
    option = input()
    int_option = int(option)
    if int_option == 1:
        result = arg1 + arg2 + arg3
        print(f'Your result is ----> {result}')
        return result
    elif int_option == 2:
        result = arg1 - arg2 - arg3
        print(f'Your result is ----> {result}')
        return result
    elif int_option == 3:
        result = arg1 * arg2 * arg3
        print(f'Your result is ----> {result}')
        return result
    elif int_option == 4:
        result = arg1 / arg2 / arg3
        print(f'Your result is ----> {result}')
        return result
calculation()
#P.S. написав примітивну функцію як для прикладу, тому без перевірок даних